export interface Note  {
  entry: string;
  date: Date
  time: {
    hour: number;
    min: number;
  };
}

